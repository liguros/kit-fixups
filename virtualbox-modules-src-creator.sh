#!/bin/bash
echo "Generate src file for virtualbox-modules ebuild v0.1"
#set -x

if [ $# -eq 0 ]
then
  # run file is from the oracle download page
  echo "Usage: $0 <run-file>"
else
  # Extract version from run file
  PV=${1#*-}
  PV=${PV%%-*}
  # Make run file executable
  chmod +x $1
  # Extract files from run file
  ./$1 --noexec --keep
  # Move into subdirectory and unpack sources
  cd install
  bzip2 -d VirtualBox.tar.bz2
  tar xvf VirtualBox.tar
  # Move into subdirectory and pack kernel sources
  cd src/vboxhost
  tar -cvf vbox-kernel-module-src-${PV}.tar *
  7z a vbox-kernel-module-src-${PV}.tar.xz vbox-kernel-module-src-${PV}.tar
  # Move source file out of subdirectories and do some cleanup
  mv vbox-kernel-module-src-${PV}.tar.xz ../../..
  cd ../../..
  rm -rf install
fi
