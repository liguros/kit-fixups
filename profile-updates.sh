#!/bin/bash

cd profiles/updates ||exit
for i in ?Q-*; do
    sed -i '/^move/d' $i
done

find . -empty |xargs -r rm -v
