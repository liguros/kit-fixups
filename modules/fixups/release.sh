#!/usr/bin/env bash

# USAGE:
# ./release.sh release commit_sha1 of gentoo_staging date_of_commit
# ./release.sh stable bdea1235c6739a5e2471b04e29f3a23980d501c4 "Nov 9, 2019"
# ./release.sh develop

FPY="foundations.py"

usage() { echo "$0 usage:" && grep "[[:space:]].)\ #" $0 | sed 's/#//' | sed -r 's/([a-z])\)/-\1/'; exit 0; }
[ $# -eq 0 ] && usage

while getopts ":hr:s:d:" arg; do
  case $arg in
    r) # Specify release, either stable or develop.
      release=${OPTARG}
      [ $release == "stable" -o $release == "develop" ] \
        && echo "Release set to $release." \
        || echo "Release needs to be either stable or develop, $release found instead."
      ;;
    s) # Specify commit_sha1 value.
      sha=${OPTARG}
      ;;
    d) # Specify date of commit.
      date=${OPTARG}
      ;;
    h | *) # Display help.
      usage
      exit 0
      ;;
  esac
done

if [[ $release == stable ]] && { [[ -z ${date+x} ]] || [[ -z ${sha+x} ]]; }; then
  echo "Date and SHA need to be set for stable release."
  usage
elif [[ $release == stable ]] && [[ $date ]] && [[ $sha ]]; then
  sed -i \
    -e "s/develop-release/stable-release/g" \
    -e "s/[0-9]*.[0-9]-release/20.7-release/g" \
    -e "s/self.stabilityRating.BETA/self.stabilityRating.PRIME/g" \
    -e 's/"default": "develop"/"default": "stable"/g' \
    $FPY

  sed -i \
    -e "s/{\"repo\": \"gentoo-staging\"},/{\"repo\": \"gentoo-staging\", \"src_sha1\": \"$sha\", \"date\": \"$date\"},/" \
    $FPY
elif [[ $release == develop ]]; then
  sed -i \
    -e "s/stable-release/develop-release/g" \
    -e "s/[0-9]*.[0-9]-release/21.1-release/g" \
    -e "s/self.stabilityRating.PRIME/self.stabilityRating.BETA/g" \
    -e 's/"default": "stable"/"default": "develop"/g' \
    $FPY

  sed -i \
    -e "s/{\"repo\": \"gentoo-staging\".*},/{\"repo\": \"gentoo-staging\"},/" \
    $FPY
fi
