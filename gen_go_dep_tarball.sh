#!/bin/bash
echo "Generate go module dependency tarball v0.3"

if [ $# -eq 0 ]
then
  echo "Usage: $0 <ebuild> <unpack_path(optional)>"
else
  # store current directory for cleanup
  RETURN_DIR="${PWD}"
  # split current path into elements to get program category and name
  IFS="/" read -ra SRC_SPLIT <<< "${PWD}" 
  ARR_LEN=${#SRC_SPLIT[@]}
  PKG_CAT=${SRC_SPLIT[${ARR_LEN} - 2]}

  if [[ -n ${2} ]]
  then
    PKG_NAME=${2}
  else
    PKG_NAME=${SRC_SPLIT[${ARR_LEN} - 1]}
  fi

  # store given ebuild name
  EBUILD="${1}"

  # remove ebuild extension
  EBUILD_P="${EBUILD%.*}"

  # get value of PORTAGE_TMPDIR
  TMP_DIR=`portageq envvar PORTAGE_TMPDIR`

  # unpack ebuild and change into unpack directory
  echo "unpack ${EBUILD}"
  ebuild ${EBUILD} clean unpack
  cd ${TMP_DIR}/portage/${PKG_CAT}/${EBUILD_P}/work/${PKG_NAME}*

  # generate dependency tarball
  echo "generate dependency tarball"
  GOMODCACHE="${PWD}"/go-mod go mod download -modcacherw
  tar -acf ${EBUILD_P}-deps.tar go-mod
  xz -z -T0 ${EBUILD_P}-deps.tar
  cp ${EBUILD_P}-deps.tar.xz ${RETURN_DIR}

  # cleanup
  cd ${RETURN_DIR}
  ebuild ${EBUILD} clean
fi
