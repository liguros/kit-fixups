Kit-Fixups Repository
=====================

This repository holds LiGurOS's custom ebuilds, profiles and fixes for branches that are necessary for maintenance of branches during their lifespan.

These fix-ups are applied over the Gentoo repository when LiGurOS's repository gets generated using the
merge-scripts [1]_.

.. [1] https://gitlab.com/liguros/merge-scripts
