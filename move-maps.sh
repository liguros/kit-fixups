#!/bin/bash

cd move-maps
rm gentoo
for i in ?Q-*; do
  sed -i -e '/^slotmove/d' -e 's/^move \(.*\) \(.*\)/\1 -> \2/g' $i
  echo "#$i" >> gentoo
  cat $i >> gentoo
done
rm ?Q-*
